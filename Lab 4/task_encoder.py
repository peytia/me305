 # -*- coding: utf-8 -*-
''' @file                       task_encoder.py
    @brief                      A task file for interacting with Quadrature Encoders
    @details                    This task file interacts with the encoder hardware while receiving
                                instruction from tank_User.py
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''

import time
import encoder
import pyb

class Encoder_task:
    ''' @brief                  Interface with quadrature encoders
        @details                This class is used to perform the actions requested by the user task to
                                interact with encoder hardware
    '''
    def __init__(self, action_Share, deltaPosition_Share):
        ''' @brief              Constructs an encoder task object
            @details            Upon instantiation, the encoder task object is created with the input parameter
                                of action_Share, which is used to communicate desired actions requested by 
                                the user task. This task then takes those requests and performs 
                                the desired action
            @param action_Share The share used to communicate action requests from the user task 
            @param deltaPosition_Share
        '''
        #encoder object instantiation, timer 4, ch1 on pin pc6 ch2 on pin pc7
        ## @brief  instantiates encoder 2
        #  @details instantiates the encoder, and allows for communication between the encoder driver and this task
        #
        self.enc_4 = encoder.Encoder(8, pyb.Pin.board.PC6, pyb.Pin.board.PC7)
        ## @brief  instantiates encoder 1
        #  @details instantiates the encoder, and allows for communication between the encoder driver and this task
        #
        self.enc_3 = encoder.Encoder(4, pyb.Pin.board.PB6, pyb.Pin.board.PB7)
        #share initialization
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        ## @brief Initializes the deltaPosition share
        #
        self.deltaPosition = deltaPosition_Share
        
        #variables used to determine the state of the data collection after pressing g declatation
        
        ## @brief  Encoder 1 collection state
        #  @details Controls whether or not the encoder is collecting data.
        #
        self.collection_state_enc1 = False
        ## @brief  Encoder 2 collection state
        #  @details Controls whether or not the encoder is collecting data.
        #
        self.collection_state_enc2 = False
        ## @brief   Timer 1
        #  @details Instantiates the timer for the 1st encoder
        #
        self.time_counter_1 = 0
        ## @brief   Timer 2
        #  @details Instantiates the timer for the 2nd encoder
        #
        self.time_counter_2 = 0
        ## @brief   Encoder 1 Position data
        #  @details Instantiate collected position data so crash doesnt occure if s or S is pressed before data collected. Also allows for the encoder data to be stored while code is in collection state.
        #
        self.collected_pos_data_enc1 = 0    
        ## @brief   Encoder 2 Position data
        #  @details Instantiate collected position data so crash doesnt occure if s or S is pressed before data collected. Also allows for the encoder data to be stored while code is in collection state.
        #
        self.collected_pos_data_enc2 = 0
        
    def encoder_update(self):
        ''' @brief              Encoder interaction task 
            @details            encoder_control is used to perform the required actions requested by the
                                user task. This includes zeroing the encoder, pulling the position of the
                                encoder, pulling the delta of the encoder, and doing a 30s data collection
                                trial for the encoder.
        '''
        self.enc_3.update()                     #update the encoder before actions take place
        self.enc_4.update()
        
        
        if self.action.read() == 1:             #the action for Zeroing  encoder 1, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.enc_3.set_position(0)          #action
        
        if self.action.read() == 12:            #the action for Zeroing encoder 2, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.enc_4.set_position(0)          #action     
        
        if self.action.read() == 2:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_3.get_position())
         
        if self.action.read() == 22:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_4.get_position())
            
        if self.action.read() == 3:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_3.get_delta())  
            
        if self.action.read() == 32:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_4.get_delta()) 
            
        if self.action.read() == 4:             #When the collection request is given, a seperate state is initialized
            self.action.write(0)
            self.collection_state_enc1 = True
            self.time_counter_1 = 0
            self.time_start_1 = time.ticks_ms()
            self.collected_pos_data_enc1 = []
            self.collected_delta_data_enc1 = []
            self.collected_time_data_enc1 = []
            
        if self.action.read() == 42:             #When the collection request is given, a seperate state is initialized
            self.action.write(0)
            self.collection_state_enc2 = True
            self.time_counter_2 = 0
            self.time_start_2 = time.ticks_ms()
            self.collected_pos_data_enc2 = [] 
            self.collected_delta_data_enc2 = []
            self.collected_time_data_enc2 = []
            
        if self.action.read() == 5:             #if a cancel condition is detected, the collection state is cancelled and returned to standby
            self.action.write(0)
            self.collection_state_enc1 = False
            self.collection_state_enc2 = False
            print(self.collected_pos_data_enc1)
            self.collected_pos_data_enc1 = []
            
            
            
        if self.collection_state_enc1 == True and self.time_counter_1 <= 30000: 
            #If the state is still valid, then the timer is refreshed and the new data is appended to the list
            self.time_stamp_1 = time.ticks_ms()
            self.time_counter_1 = time.ticks_diff(self.time_stamp_1,self.time_start_1)            
            self.collected_pos_data_enc1.append(self.enc_3.get_position())
            self.collected_delta_data_enc1.append(self.enc_3.get_position())
            self.collected_time_data_enc1.append(self.time_counter_1)
            
            print(self.time_counter_1,self.enc_3.get_position(),self.enc_3.get_delta())
        
        if self.collection_state_enc2 == True and self.time_counter_2 <= 30000: 
            #If the state is still valid, then the timer is refreshed and the new data is appended to the list
            self.time_stamp_2 = time.ticks_ms()
            self.time_counter_2 = time.ticks_diff(self.time_stamp_2,self.time_start_2)            
            self.collected_pos_data_enc2.append(self.enc_4.get_position())
            self.collected_delta_data_enc2.append(self.enc_4.get_position())
            self.collected_time_data_enc2.append(self.time_counter_2)
            
            print(self.time_counter_2,self.enc_4.get_position(),self.enc_4.get_delta())
        
        
        if self.collection_state_enc1 == True and self.time_counter_1 >= 30000:
            #If the state is over time, then it is terminated and the output is given, then the used variables are cleared.
            self.collection_state_enc1 = False
            print(self.collected_time_data_enc1, self.collected_pos_data_enc1,self.collected_delta_data_enc1)
            self.collected_time_data_enc1 = []
            self.collected_pos_data_enc1 = []
            self.collected_delta_data_enc1 = []
            
        if self.collection_state_enc2 == True and self.time_counter_2 >= 30000:
            #If the state is over time, then it is terminated and the output is given, then the used variables are cleared.
            self.collection_state_enc2 = False
            print(self.collected_time_data_enc2, self.collected_pos_data_enc2,self.collected_delta_data_enc2)
            self.collected_pos_data_enc2 = []
            self.collected_delta_data_enc2 = []
            self.collected_delta_data_enc2 = []
            
        self.deltaPosition.write(self.enc_3.get_delta()*2*3.14159/1000)             #returns a rad/s value using conversion