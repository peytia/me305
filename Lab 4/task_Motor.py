# -*- coding: utf-8 -*-
''' @file                       task_Motor.py
    @brief                      A task file for interacting with motors
    @details                    This task file interacts with users wanting to take information off of an
                                encoder. This gives instruction to the encoder task which then interacts with
                                the encoder hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 28, 2021
'''
import pyb
import DRV8847
import ClosedLoop
import time

class Motor_task:
    ''' @brief                  Interface with motors
        @details                This class is used to perform the actions requested by the user task to
                                interact with motor hardware
    '''
    def __init__(self, action_Share, duty_Share, deltaPosition_Share, gain_Share, setPoint_Share):
        ''' @brief              Constructs an encoder task object
            @details           
            @param action_Share   The share used to communicate action requests from the user task 
            @param duty_Share     The share used to communicate the duty cycle value to the motor 
            @param deltaPosition  The share used to communicate the change in position. 
            @param gain_Share     The share used to communicate the gain and its changes throughout the tasks.
            @param setPoint_Share The share used to communicate the set velocity from the user task. 
        '''
       
        #share initialization
        ## @brief   Variable for the duty share
        #  @details Initializes the duty share and allows for the share to be used within this task.
        #
        self.duty = duty_Share
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        
        #motor driver and motor instantiation
        
        ## @brief   Motor Driver
        #  @details Instantiates the motor driver, and allows for it to be manipulated through this task.
        #
        self.motor_drv     = DRV8847.DRV8847(pyb.Pin.cpu.A15)                   #first DRV is file name, second DRV is class instantiation
        ## @brief   Motor 1
        #  @details Instantiates the 1st motor allows for the duty cycle for the motor to be set.
        #
        self.motor_1       = self.motor_drv.motor(pyb.Pin.cpu.B4,pyb.Pin.cpu.B5, 1, 2)
        ## @brief   Motor 2
        #  @details Instantiates the 2nd motor allows for the duty cycle for the motor to be set.
        #
        self.motor_2       = self.motor_drv.motor(pyb.Pin.cpu.B0,pyb.Pin.cpu.B1, 3, 4)
        
        self.motor_drv.enable()
        
        #collection loop check instantiation
        ## @brief sets the data collection state for the controller.
        #
        self.controllerCollection_state = False
        ## @brief instantiates the timer for the controller
        #
        self.time_counter_control = 0
        
    def motor_update(self):
        ''' @brief              Motor interaction task 
            @details            
        '''
        if self.action.read() == 6:             #the action for Zeroing  encoder 1, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            print(self.duty.read())
            self.motor_1.set_duty(self.duty.read())    #action
            #self.duty.write(0)                     #clear duty share
        
        if self.action.read() == 62:            #the action for Zeroing encoder 2, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.motor_2.set_duty(self.duty.read())    #action
            #self.duty.write(0)
            
        if self.action.read() == 7:             #Clear Fault in Motor
            self.motor_drv.fault_cb(0)
            
        if self.controllerCollection_state == True:
            self.L = self.controller.update_closed_loop(self.deltaPos.read())
            print(self.L)
            self.motor_1.set_duty(self.L)
            
   
    def controller_update(self): 
        
        if self.action.read() == 8:
            self.action.write(0)
            self.controller.set_Kp(self.newKp.read())
        if self.action.read() == 9:
            self.action.write(0)
            self.controller.set_omega(self.setPoint.read())
            self.controllerCollection_state = True
            self.time_counter_control = 0
            self.time_start_control = time.ticks_ms()
            self.collected_omega_data_control = []
            self.collected_time_data_control = []
            
        if self.controllerCollection_state == True and self.time_counter_control <= 10000:
            self.time_stamp_control = time.ticks_ms()
            self.time_counter_control = time.ticks_diff(self.time_stamp_control,self.time_start_control)            
            self.collected_omega_data_control.append(self.controller.get_omega())
            self.collected_time_data_control.append(self.time_counter_control)
            print(self.time_counter_control, self.controller.get_omega())
        if self.controllerCollection_state == True and self.time_counter_control >= 10000:    
            self.controllerCollection_state = False
            
            print(self.collected_time_data_control, self.collected_omega_data_control)