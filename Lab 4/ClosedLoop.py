# -*- coding: utf-8 -*-
''' @file ClosedLoop.py
    @brief                      A for creating a closed loop
    @details                    This file interacts with the encoder and motor tasks to implement a closed loop control to the system.
                             
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import time

class ClosedLoop:
    ''' @brief     A controller driver meant to utilize a closed loop
        @details   This class enables for the system to operate under closed loop. This driver also allows for the gain of the system to be adjusted.
                    
    '''
    def __init__(self, Kp, deltaPosition_share):
        ''' @brief  Initializes and returns a ClosedLoop object
            @param  Kp                   Gain of the controller
            @param  deltaPosition_share  The share used to communicate the change in position. 
    
        '''
        ## @brief Initializes the deltaPosition share
        #
        self.currentDelta = deltaPosition_share
        ## @brief Initializes the current gain of the system
        #
        self.currentKp = Kp
        ## @brief Initializes the timer
        #
        self.time = time.ticks_us()
        ## @brief Sets desired angle
        #
        self.desiredOmega = 0
        
        pass
    
    def update_closed_loop (self, deltaPos):     #in rad/s
        ''' @brief updates the closed loop 
            @param deltaPos The the change in position of the system.
        '''
        ## @brief records time
        #
        self.previousTime = self.time
        ## @brief records current time
        ## @details these two moments in time are used to calculate the change in time
        #
        self.currentTime = time.ticks_us()
        ## @brief Calculates change in time.
        #
        self.deltaT = time.ticks_diff(self.previousTime,self.currentTime)
        self.previousTime = self.currentTime
        ## @brief records the current omega.
        #
        self.currentOmega = deltaPos/self.deltaT
        
        #print(self.desiredOmega,self.currentOmega)
        
        ## @brief Sets desired L. This is the percentage by which the PWM changes. 
        #
        self.L_desired = 0.1*self.currentKp*(self.desiredOmega - self.currentOmega)
        if self.L_desired > 100:
            self.L = 100
        elif self.L_desired < -100:
            self.L = -100
        else:
            self.L = self.L_desired
        #print(self.L_desired, self.L)
        return self.L
    
    def get_Kp (self):
        ''' @brief   
        '''

        return self.currentKp
    
    def set_Kp (self, newKp):
        ''' @brief   
        '''
        self.currentKp = newKp
        pass
    
    def set_omega (self, Omega):
        ''' @brief   
        '''
        self.desiredOmega = Omega
    def get_omega (self):
        return self.currentOmega

minn = -100
maxn = 100
