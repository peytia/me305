# -*- coding: utf-8 -*-
''' @file                       task_User.py
    @brief                      A task file for interacting with users
    @details                    This task file interacts with users wanting to take information off of an
                                encoder. This gives instruction to the encoder task which then interacts with
                                the encoder hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import pyb

class User_task:
    ''' @brief                  Interface with users
        @details                This class is used to interact with users wanting to take information off
                                encoder hardware
    '''
    def __init__(self, action_Share):
        ''' @brief              Constructs an encoder task object
            @details            Upon instantiation, the user task object is created with the input parameter
                                of action_Share, which is used to communicate desired actions requested by 
                                the user to the encoder task. 
            @param action_Share The share used to communicate action requests to the encoder task
        '''
        self.action = action_Share
        self.ser_port = pyb.USB_VCP()
        
        print ('Encoder Testing Interface')
        print ('press z ---- Zero Encoder position')
        print ('press p ---- Display Encoder position')
        print ('press d ---- Display Encoder delta value')  
        print ('press g ---- Collect data from encoder for 30 seconds')
        print ('press s ---- Cancel data collection ')
        
    def User_input(self):
        if self.ser_port.any():
            self.user_in = self.ser_port.read(1)
            
            if self.user_in == b'z':
                print('Zeroing Encoder Position')
                self.action.write(1)
                
            elif self.user_in == b'p':
                print('Printing Position Value')
                self.action.write(2)
                
            elif self.user_in == b'd':
                print('Printing Delta Value')
                self.action.write(3)
                
            elif self.user_in == b'g':
                print('Beginning Data Collection')
                self.action.write(4)
                
            elif self.user_in == b's':
                print('Data Collection Cancelled')
                self.action.write(5)