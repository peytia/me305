# -*- coding: utf-8 -*-
''' @file                       main.py
    @brief                      A main file for testing or interating directly with encoders
    @details                    This main file initiates and calls on the tasks for performing their respective
                                tasks with interacting with users and hardware.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import task_encoder
import task_user
import time
import shares

action_Share = shares.Share(0)   #A share for passing request information from the user task to encoder task

user = task_user.User_task(action_Share)            #passing action_share into both tasks
encoder = task_encoder.Encoder_task(action_Share)   
if __name__ == '__main__':    
    while True:         #main while loop
        startTime = time.ticks_us()
        user.User_input()
        encoder.encoder_control()
        stopTime = time.ticks_us()
        time.sleep((50000-time.ticks_diff(stopTime,startTime))/1000000) #used for regulating time of tasks to 20Hz