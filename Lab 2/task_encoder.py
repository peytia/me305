# -*- coding: utf-8 -*-
''' @file                       task_encoder.py
    @brief                      A task file for interacting with Quadrature Encoders
    @details                    This task file interacts with the encoder hardware while receiving
                                instruction from tank_User.py
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''

import time
import encoder
import pyb

class Encoder_task:
    ''' @brief                  Interface with quadrature encoders
        @details                This class is used to perform the actions requested by the user task to
                                interact with encoder hardware
    '''
    def __init__(self, action_Share):
        ''' @brief              Constructs an encoder task object
            @details            Upon instantiation, the encoder task object is created with the input parameter
                                of action_Share, which is used to communicate desired actions requested by 
                                the user task. This task then takes those requests and performs 
                                the desired action
            @param action_Share The share used to communicate action requests from the user task 
        '''
        #encoder object instantiation, timer 4, ch1 on pin pc6 ch2 on pin pc7
        self.enc_3 = encoder.Encoder(3, pyb.Pin.board.PC6, pyb.Pin.board.PC7) 
        
        #share initialization
        self.action = action_Share
        
        #variables used to determine the state of the data collection after pressing g declatation
        self.collection_state = False
        self.time_counter = 0
    def encoder_control(self):
        ''' @brief              Encoder interaction task 
            @details            encoder_control is used to perform the required actions requested by the
                                user task. This includes zeroing the encoder, pulling the position of the
                                encoder, pulling the delta of the encoder, and doing a 30s data collection
                                trial for the encoder.
        '''
        self.enc_3.update()                     #update the encoder before actions take place
        if self.action.read() == 1:             #the action for Zeroing the encoder, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.enc_3.set_position(0)          #action
            
        if self.action.read() == 2:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_3.get_position())
            
        if self.action.read() == 3:             #Follows similar format to if statement above
            self.action.write(0)
            print(self.enc_3.get_delta())  
            
        if self.action.read() == 4:             #When the collection request is given, a seperate state is initialized
            self.action.write(0)
            self.collection_state = True
            self.time_counter = 0
            self.time_start = time.ticks_ms()
            self.collected_data = []
            
        if self.action.read() == 5:             #if a cancel condition is detected, the collection state is cancelled and returned to standby
            self.action.write(0)
            self.collection_state = False
            print(self.collected_data)
            self.collected_data = []
            
            
            
        if self.collection_state == True and self.time_counter <= 30000: 
            #If the state is still valid, then the timer is refreshed and the new data is appended to the list
            self.time_stamp = time.ticks_ms()
            self.time_counter = time.ticks_diff(self.time_stamp,self.time_start)            
            self.collected_data.append(self.enc_3.get_position())
            print(self.time_counter,self.enc_3.get_position())
        if self.collection_state == True and self.time_counter >= 30000:
            #If the state is over time, then it is terminated and the output is given, then the used variables are cleared.
            self.collection_state = False
            print(self.collected_data)
            self.collected_data = []
