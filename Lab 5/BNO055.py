# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 13:38:08 2021

@author: melab15
"""

# -*- coding: utf-8 -*-
''' @file DRV8847.py
'''

class BNO055:
    ''' @brief      A motor driver class for the DRV8847 from TI.6
        @details    Objects of this class can be used to configure the DRV88477
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor
                    control.
                    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, I2C_Channel):
        ''' @brief      Initializes and returns a DRV8847 object.
        '''
       
        self.i2c = pyb.I2C(I2C_Channel, pyb.I2C.MASTER)
    def oper_mode (self, operation_mode, I2C_Address):
        
        self.i2c.mem_write(operation_mode, I2C_Address, 0x3D)
        
        
        
        pass
    def calibStatus (self):
        ''' @brief      Brings the DRV8847 out of sleep mode.
        '''
                
        cal_bytes =  self.i2c.mem_read(1, 0x28, 0x35)


        cal_status = ( cal_bytes[0] & 0b11,
                       (cal_bytes[0] & 0b11 << 2) >> 2,
                       (cal_bytes[0] & 0b11 << 4) >> 4,
                       (cal_bytes[0] & 0b11 << 6) >> 6)

        print('\n')

        return cal_status

    def calibConstant (self):
        ''' @brief      Puts the DRV8847 in sleep mode.
        '''
        self.bufcal = bytearray(22)
        return self.i2c.mem_read(self.bufcal, 0x28, 0x55)
#    def calibWrite (self, cal_con):
#        cal_bytes = bytearray(cal_con)
    
    def EulerAngle (self):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @return     An object of class Motor
        '''
        self.buffer_euler = bytearray(6)
        self.eulerAng_hexval = self.i2c.mem_read(self.buffer_euler, 0x28, 0x1A)
        self.eulerAng_unscaled = struct.unpack('<hhh', self.eulerAng_hexval)
        self.eulerAng = tuple(eul_int/16 for eul_int in self.eulerAng_unscaled)
    
    

        return self.eulerAng
    
    def Acceleration (self):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @return     An object of class Motor
        '''
        self.buffer_accel = bytearray(6)
        self.acc_hexval = self.i2c.mem_read(self.buffer_accel, 0x28, 0x08)
        self.acc_unscaled = struct.unpack('<hhh', self.acc_hexval)
        self.acc = tuple(acc_int/16 for acc_int in self.acc_unscaled)
        
        return self.acc


    
if __name__ == '__main__':
    import pyb
    import struct
    import time

    
    
    Accell = BNO055(1)
    Accell.oper_mode(12,0x28)
    state = 0
    while True:
        
        if state == 0: 
            Calibration_Status = Accell.calibStatus()
            print(Calibration_Status)
            if Calibration_Status == (3,3,3,3):
                print('Fully Calibrated')
                state = 1
        if state == 1:
            eulerData = Accell.EulerAngle()
            print(eulerData)
        time.sleep_ms(500)
        