# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 15:27:08 2021

@author: melab15
"""

import pyb
import time
import struct

i2c = pyb.I2C(1, pyb.I2C.MASTER)

i2c.mem_write(1, 0x28, 0b00001000)
#i2c.mem_write(1, 0x28, 0x3B, 0b00000000)

calStatus = i2c.mem_read(1, 0x28, 0x35)
print('{:#010b}'.format(calStatus[0]))
    
bufcal = bytearray(22)
cal_const = i2c.mem_read(bufcal, 0x28, 0x55)
print(cal_const)

bufeu = bytearray(6)
bufacc = bytearray(6)
while True:   
    eulerAng_hexval = i2c.mem_read(bufeu, 0x28, 0x1A)
    eulerAng_unscaled = struct.unpack('<hhh', eulerAng_hexval)
    eulerAng = tuple(eul_int/16 for eul_int in eulerAng_unscaled)
    
    
    acc_hexval = i2c.mem_read(bufacc, 0x28, 0x08)
    acc_unscaled = struct.unpack('<hhh', acc_hexval)
    acc = tuple(acc_int/16 for acc_int in acc_unscaled)
    print(eulerAng, acc)
    time.sleep_ms(500)
