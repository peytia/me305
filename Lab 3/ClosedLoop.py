# -*- coding: utf-8 -*-
"""
Created on Sat Nov 13 16:12:09 2021

@author: kepte
"""
import time

class ClosedLoop:
    ''' @brief     
        @details    
                    
    '''
    def __init__(self, Kp, deltaPosition_share):
        ''' @brief   
        '''
        self.currentDelta = deltaPosition_share
        self.currentKp = Kp
        self.time = time.ticks_us()
        
        self.desiredOmega = 0
        
        pass
    
    def update_closed_loop (self):
        ''' @brief   
        '''
        self.previousTime = self.time
        self.currentTime = time.ticks_us()
        self.deltaT = time.ticks_diff(self.previousTime,self.currentTime)
        self.previousTime = self.currentTime
        
        self.currentOmega = self.currentDelta.read()/self.deltaT
        print(self.currentOmega)
        pass
    
    def get_Kp (self):
        ''' @brief   
        '''
        return self.currentKp
    
    def set_Kp (self, newKp):
        ''' @brief   
        '''
        self.currentKp = newKp
        pass

minn = -100
maxn = 100
