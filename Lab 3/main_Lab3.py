# -*- coding: utf-8 -*-
''' @file                       main_Lab3.py
    @brief                      A main file for testing or interating directly with encoders
    @details                    
                                This main file initiates and calls on the tasks for performing their respective
                                tasks with interacting with users and hardware. When ran, it will go to a standby state
                                where it displays what a user can type into the PuTTY prompt. This includes interacting with the
                                encoder as well as the motor. The motor position can be plotted by running the motor and then using
                                the encoder collection function which generated the following Plots.
                                
                                \image html Lab3PositionGraph1.PNG width=700 
                                \image html Lab3PositionGraph2.PNG width=700 
                                
                                Before this code was created, Finite State Machines were used to lay out the logic of the system being created.
                                This can be seen with the following images:

                                \image html EncoderTaskDiagramLab4.PNG width=700 "State Machine used for the Encoder Task" 
                                \image html TaskUserdiagramLab3.PNG width=700 "State Machine used for the User Task" 
                                \image html MotorTaskLab3.PNG width=700 "State Machine used for the Motor Task"
                                \image html Lab3TD.png width=700 "Task Diagram used for the Project" 
			 
     
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       November 17, 2021
'''
import task_encoder
import task_user
import task_motor
import time
import shares

## @brief     Instantiates action_Share
#  @details   A share for passing request information from the user task to encoder task
#
action_Share = shares.Share(0)    
## @brief     Instantiates action_Share
#  @details   A share for the Duty Cycle request from the User Input to the Motor Task
#  
duty_Share = shares.Share(0)       
## @brief    Instantiates the user task
#  @details  Passing action_share and duty_Share into the user task.
#
user = task_user.User_task(action_Share, duty_Share)            
## @brief    Instantiates the encoder task
#  @details  Passing action_share and into the encoder task.
#
encoder = task_encoder.Encoder_task(action_Share) 
## @brief    Instantiates the motor task
#  @details  Passing action_share and duty share into the motor task.
#
motor = task_motor.Motor_task(action_Share, duty_Share)
 
if __name__ == '__main__':    
    while True:         #main while loop
        ## @brief Timer
        #  @details Instantiates the timer and starts recording it.
        #

        startTime = time.ticks_us()
        user.User_input()
        encoder.encoder_update()
        
        motor.motor_update()
        ## @brief  Stop Timer
        #  @details Instantiates the stop timer and starts recording it.
        #
        stopTime = time.ticks_us()
        time.sleep((50000-time.ticks_diff(stopTime,startTime))/1000000) #used for regulating time of tasks to 20Hz