# -*- coding: utf-8 -*-
''' @file DRV8847.py
    @brief                      A driver for the motors
    @details                    This is a driver for interfacing with the motors. Takes in inputs to correctly configure the pins and channels.
                                 
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import pyb
import time

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.6
        @details    Objects of this class can be used to configure the DRV88477
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor
                    control.
                    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, SleepPin):
        ''' @brief      Initializes and returns a DRV8847 object.
            @param      SleepPin The pin resposible for determining the state of the motor. 
        '''
        ## @brief instantiates the sleep pin of the motor
        #
        self.SleepState = pyb.Pin (SleepPin, pyb.Pin.OUT_PP)
        self.SleepState.low()
        ## @brief instantiates the fault pin of the motor
        #
        self.nfault = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN)
        ## @brief determines whether or not the motor fault conditions are enabled or disabled.
        #
        self.fault_int = pyb.ExtInt(self.nfault, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        pass
    def enable (self):
        ''' @brief      Brings the DRV8847 out of sleep mode.
        '''
        self.fault_int.disable()
        self.SleepState.high()
        time.sleep_us(25)
        self.fault_int.enable()
        pass

    def disable (self):
        ''' @brief      Puts the DRV8847 in sleep mode.
        '''
        self.SleepState.low()
        pass
    def fault_cb (self, IRQ_src):
        ''' @brief      Callback function to run on fault condition.
            @param      IRQ_src The source of the interrupt request.
        '''
        self.fault_int.enable()
        pass
    def motor (self, In1Pin, In2Pin, In1Channel, In2Channel):
        ''' @brief        Initializes and returns a motor object associated with the DRV8847.
            @param In1Pin Motor Pin 1
            @param In2Pin Motor Pin 2
            @param In1Channel Motor Channel 1
            @param In2Channel Motor Channel 2
            @return       An object of class Motor
        '''
        return Motor(In1Pin, In2Pin, In1Channel, In2Channel)

class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    
    def __init__ (self, In1Pin, In2Pin, In1Channel, In2Channel):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
            @param      In1Pin Motor Pin 1
            @param      In2Pin Motor Pin 2
            @param      In1Channel Motor Channel 1
            @param      In2Channel Motor Channel 2
        '''
        
        #self.CCW_Pin = pyb.Pin (In1Pin, pyb.Pin.OUT_PP)
        #self.CW_Pin = pyb.Pin (In2Pin, pyb.Pin.OUT_PP)
        ## @brief Instantiates the motor timer.
        #
        self.motorTimer = pyb.Timer (3, freq = 20000)
        ## @brief Sets the motor channels and pins to spin in the Counterclockwise direction
        #
        self.CCW_channel = self.motorTimer.channel(In1Channel, pyb.Timer.PWM, pin=In1Pin)
        ## @brief Sets the motor channels and pins to spin in the clockwise direction
        #
        self.CW_channel = self.motorTimer.channel(In2Channel, pyb.Timer.PWM, pin=In2Pin)
        
        self.CW_channel.pulse_width_percent(0)
        self.CCW_channel.pulse_width_percent(0)
        
        pass
    
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor
        '''
        if duty > 100 or duty < -100:
            print('Error, Duty Cycle out of Range')
        elif duty > 0:
            self.CW_channel.pulse_width_percent(0)
            self.CCW_channel.pulse_width_percent(duty)
        elif duty < 0:
            self.CCW_channel.pulse_width_percent(0)
            self.CW_channel.pulse_width_percent(-duty)
        elif duty == 0:
            self.CCW_channel.pulse_width_percent(0)
            self.CW_channel.pulse_width_percent(0)
        pass
    
if __name__ =='__main__':  #Sets up motor pins and allows for the motor to recieve input.
       
    ## @brief   Motor Driver
    #  @details Instantiates the motor driver and sets the correct pins.
    #
    motor_drv     = DRV8847(pyb.Pin.cpu.A15)
    ## @brief   Motor 1
    #  @details Instantiates the 1st motor sets the corresponding pins.
    #
    motor_1       = motor_drv.motor(pyb.Pin.cpu.B4,pyb.Pin.cpu.B5, 1, 2)
    ## @brief   Motor 2
    #  @details Instantiates the 2nd motor sets the corresponding pins.
    #
    motor_2       = motor_drv.motor(pyb.Pin.cpu.B0,pyb.Pin.cpu.B1, 3, 4)
    
    # Enable the motor driver
    motor_drv.enable()
    
    
    # print('CCW Soft')
    # motor_1.set_duty(90)
    
    # time.sleep(2)
    # print('CW Hard')
    # motor_1.set_duty(-90)
    
    # time.sleep(2)
    # print('Stop')
    # motor_1.set_duty(0)
    
    
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    #motor_1.set_duty(40)
    #motor_2.set_duty(60)