# -*- coding: utf-8 -*-
''' @file                       task_Motor.py
    @brief                      A task file for interacting with motors
    @details                    This task file interacts with users wanting to take information off of an
                                encoder. This gives instruction to the encoder task which then interacts with
                                the encoder hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 28, 2021
'''
import pyb
import DRV8847



class Motor_task:
    ''' @brief                  Interface with motors
        @details                This class is used to perform the actions requested by the user task to
                                interact with motor hardware
    '''
    def __init__(self, action_Share, duty_Share):
        ''' @brief              Constructs an encoder task object
            @details           
            @param action_Share The share used to communicate action requests from the user task 
            @param duty_Share   The share used to communicate the duty cycle value to the motor 
        '''
       
        #share initialization
        
        ## @brief   Variable for the duty share
        #  @details Initializes the duty share and allows for the share to be used within this task.
        #
        self.duty = duty_Share
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        
        #motor driver and motor instantiation
        
        ## @brief   Motor Driver
        #  @details Instantiates the motor driver, and allows for it to be manipulated through this task.
        #
        self.motor_drv     = DRV8847.DRV8847(pyb.Pin.cpu.A15)                   #first DRV is file name, second DRV is class instantiation
        ## @brief   Motor 1
        #  @details Instantiates the 1st motor allows for the duty cycle for the motor to be set.
        #
        self.motor_1       = self.motor_drv.motor(pyb.Pin.cpu.B4,pyb.Pin.cpu.B5, 1, 2)
        ## @brief   Motor 2
        #  @details Instantiates the 2nd motor allows for the duty cycle for the motor to be set.
        #
        self.motor_2       = self.motor_drv.motor(pyb.Pin.cpu.B0,pyb.Pin.cpu.B1, 3, 4)
        
        self.motor_drv.enable()
        
    def motor_update(self):
        ''' @brief              Motor interaction function
            @details            Updates the motor parameters. Allows for the duty cycle to be updated, as well as any motor faults to be cleared.
        '''
        if self.action.read() == 6:             #the action for Zeroing  encoder 1, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            print(self.duty.read())
            self.motor_1.set_duty(self.duty.read())    #action
            self.duty.write(0)                     #clear duty share
        
        if self.action.read() == 62:            #the action for Zeroing encoder 2, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.motor_2.set_duty(self.duty.read())    #action
            self.duty.write(0)
            
        if self.action.read() == 7:             #Clear Fault in Motor
            self.motor_drv.fault_cb(0)

            
        
        
   