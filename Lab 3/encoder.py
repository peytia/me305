# -*- coding: utf-8 -*-

''' @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details                    This is a driver for interfacing with Quadrature Encoders. This driver
                                needs input parameters of the timer which is the proper timer that the
                                encoder uses and the 2 channel pins which the encoder outputs.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders
        @details                This is a driver for interfacing with Quadrature Encoders. This driver
                                needs input parameters of the timer which is the proper timer that the
                                encoder uses and the 2 channel pins which the encoder outputs.
    '''
    
    def __init__(self,timer_ch, channel1pin, channel2pin):
        ''' @brief              Constructs an encoder object
            @details            Upon instantiation, the encoder object is created with the input parameters
                                of the timer and channel pins which the encoder outputs on. Additionally,
                                the position, count, and delta variables are created and zeroed.
            @param  timer_ch    The timer number which the encoder uses
            @param  channel1pin The pin which channel 1 output is on
            @param  channel1pin The pin which channel 2 output is on. 
        '''
        ## @brief timer
        #  @details  instantiates the timer for the encoder
        #
        self.tim = pyb.Timer(timer_ch, prescaler=0, period=65535)
        self.ch1 = self.tim.channel(1, pyb.Timer.ENC_A, pin=channel1pin)
        self.ch2 = self.tim.channel(2, pyb.Timer.ENC_B, pin=channel2pin)
        ##  @brief Encoder velocity
        #   @details Takes value for delta from the encoder.
        #
        self.delta = 0
        ##  @brief counter
        #   @details starts counting using the timer.
        #
        self.count = 0
        ##  @brief Encoder Position
        #   @details Takes value for the position from the encoder.
        #
        self.position = 0
        
    def update(self):
        ''' @brief              Updates encoder position and delta
            @details            Update uses the counter from the encoder to update the known position of the encoder and 
                                check if increase in counts is greater than half of the period to ensure that the position 
                                of the encoder is acurate and does not experience an overload.
        '''
        
        ##  @brief Records Start
        #   @details records the start time for the counter.
        #
        self.initialCount = self.count
        self.count = self.tim.counter()
        self.delta = self.count - self.initialCount
        self.initialCount = self.count
        if self.delta >= 65535/2:
            self.delta -= 65535
        if self.delta <= -65535/2:
            self.delta += 65535
        self.position += self.delta
        
        
    def get_position(self):
        ''' @brief              Returns encoder position
            @details            get_position returns the current recorded position of the encoder. This task 
                                should be performed directly after the encoder position has been updated with 
                                encoder.update() for the most accurate return.
            @return             The position of the encoder shaft
        '''
        return self.position
    def set_position(self, inputPosition):
        ''' @brief              Sets encoder position
            @details            set_position sets the position to a given value. This can be used to zero the
                                position of the encoder by passing a 0 value through the input parameter
            @param  position    The new position of the encoder shaft
        '''
        self.position = inputPosition
        self.delta = 0
    
    def get_delta(self):
        ''' @brief              Returns encoder delta
            @details            get_delta returns the delta value, or change per timestep. This is can be used to
                                to compute angular velocity if the timestep is known.
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        '''
        return self.delta