# -*- coding: utf-8 -*-
''' @file                       task_User.py
    @brief                      A task file for interacting with users
    @details                    This task file interacts with users wanting to take information off of an
                                encoder. This gives instruction to the encoder task which then interacts with
                                the encoder hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import pyb

class User_task:
    '''                                                 @brief                  Interface with users
                                                        @details                This class is used to interact with users wanting to take information off
                                                                                encoder hardware
    '''
    def __init__(self, action_Share, duty_Share):
        '''                                             @brief              Constructs an encoder task object
                                                        @details            Upon instantiation, the user task object is created with the input parameter
                                                                            of action_Share, which is used to communicate desired actions requested by 
                                                                            the user to the encoder task. 
                                                        @param action_Share The share used to communicate action requests to the encoder task
                                                        @param duty_Share   The share used to communicate the duty level requested to the motor task
        '''
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        ## @brief   Variable for the duty share
        #  @details Initializes the duty share and allows for the share to be used within this task.
        #
        self.duty = duty_Share
        ## @brief   Variable for the USB port
        #  @details Initializes the USB port and allows for the code to transfer information through this port
        #
        self.ser_port = pyb.USB_VCP()
                                                        ##  @brief     collectionState is used to switch collection states.
                                                        #   @details   Allows for the user task to be switched into a collective state where the user inputs values for the duty of motors. 
                                                        #
        self.collectionState = 0
       
         
        
        print ('Encoder Testing Interface')
        print ('press z ---- Zero Encoder position')
        print ('press Z ---- Zero Encoder position')
        print ('press p ---- Display Encoder position')
        print ('press P ---- Display Encoder position')
        print ('press d ---- Display Encoder delta value') 
        print ('press D ---- Display Encoder delta value') 
        print ('press m ---- Enter duty cycle for motor 1') 
        print ('press M ---- Enter duty cycle for motor 2') 
        print ('press c or C ---- Clear fault') 
        print ('press g ---- Collect data from encoder 1 for 30 seconds')
        print ('press G ---- Collect data from encoder 2 for 30 seconds')
        print ('press s or S ---- Cancel data collection ')
            
        
    def User_input(self):
        '''                                             @brief              Designates shares based on user input.
                                                        @details            After the user has been presented with the prompts, user will input the corresponding character
                                                                            to the desired action. The action share is then modified to be shared to the appropriate task.                                                                              
        '''
        if self.ser_port.any():
            
                                                        ## @brief    Collects the user input
                                                        #  @details  Once the user is prompted for an input, this variable is changed based off of the input of the user. The shares are then changed based off of the new value of the variable, and program behaves accordingly.
                                                        #
            self.user_in = self.ser_port.read(1)
            
            if self.user_in == b'z':                                        # Sets encoder 1 to zero position
                print('Zeroing Encoder 1 Position')
                self.action.write(1)
            
            elif self.user_in == b'Z':                                      # Sets encoder 2 to zero position
                print('Zeroing Encoder 2 Position')
                self.action.write(12)    
            
            elif self.user_in == b'p':                                      # Prints current encoder 1 position
                print('Printing Position Value')
                self.action.write(2)
            
            elif self.user_in == b'P':                                      # Prints current encoder 2 position
                print('Printing Position Value')
                self.action.write(22)    
           
            elif self.user_in == b'd':                                      # Prints current encoder 1 delta
                print('Printing Encoder 1 Delta Value')
                self.action.write(3)
             
            elif self.user_in == b'D':                                      # Prints current encoder 2 delta
                print('Printing Encoder 2 Delta Value')
                self.action.write(32)    
                
            elif self.user_in == b'g':                                      # Starts encoder 1 data collection for 30 seconds
                print('Beginning Encoder 1 Data Collection')
                self.action.write(4)
            
            elif self.user_in == b'G':                                      # Starts encoder 2 data collection for 30 seconds
                print('Beginning Encoder 2 Data Collection')     
                self.action.write(42)    
            
            elif self.user_in == b's':                                      # Halts data collection for both encoders
                print('Data Collection Cancelled')
                self.action.write(5)
           
            elif self.user_in == b'S':                                      # Halts data collection for both encoders
                print('Data Collection Cancelled') 
                self.action.write(5)
            
            elif self.user_in == b'm':                                      # Prompts user to input value for motor 1 duty cycle
                print('Setting duty cycle for motor 1')
                
                ## @brief      Sets motor duty cycles.
                #  @details    If the characters that the user inputs are numeric values that meet the duty cycle requirements, then the motor duty cycle is set to the input value.
                #
                self.numStr = ''              
                self.collectionState = 1
            
            elif self.user_in == b'M':                                      # Prompts user to input value for motor 2 duty cycle
                print('Setting duty cycle for motor 2')
                self.numStr = ''        
                self.collectionState = 2
            
            elif self.user_in == b'c':                                      # Clears Fault condition for both motors
                print('Fault Condition cleared')
                self.action.write(7)
                
            elif self.user_in == b'C':                                      # Clears Fault condition for both motors
                print('Fault Condition cleared')
                self.action.write(7)
                
                
            if self.collectionState > 0:
                
                ## @brief           Takes in the user input for the duty cycle.
                #  @details         Once the task is switched to collection state, this variable will take in the user character input for the duty cycle.
                #
                self.char_in = self.user_in.decode()
                if self.char_in.isdigit() == True:
                    self.numStr += self.char_in
                    self.ser_port.write(self.char_in)
                if self.char_in == '-' and self.numStr == '':
                    self.numStr += self.char_in
                    self.ser_port.write(self.char_in)
                if self.char_in == '\x7f':
                    self.numStr = self.numStr[:-1]
                    self.ser_port.write(self.char_in)
                if self.char_in == '\r' or self.char_in == '\n':
                    self.number = float(self.numStr)
                    print('\n')
                    if self.collectionState == 1 or self.collectionState == 2:
                        self.duty.write(self.number)
                        if self.collectionState == 1:
                            self.action.write(6)
                        if self.collectionState == 2:
                            self.action.write(62)
                        self.collectionState = 0
                        print("Duty Cycle Set to", self.numStr)
                        
                