# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 15:18:59 2021
@file Lab1.py
@brief Takes user input in the form of a button press and switches between stages with different waveforms for the LED
@description Allows for interaction with board through Putty. Prompts user for input and goes on standby until input is recieved when the button is pressed. Once it is pressed it switches into stage 2, which contains a square wave. The LED then lights up in the square wave pattern. The button is pressed again and the LED behaves in a sine wave pattern. The button is pressed again and the led behaves in a sawtooth wave pattern. The button is pressed again and the cycle repeats, with the LED behaving in a square wave pattern. Each time the button is pressed the wave resets and each stage starts at the desired start time of the wave.

@author: Caleb Kephart
@author: Peyton Archibald
@date: 10/7/2021
@copyright license info. 

"""
import time
import utime
import math

#all functions should be defined before the if __name__ ... block below

import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

def onButtonPressFCN(IRQ_src):
    ''' @brief                                  Allows for button input
        @details                                Sets the value of button to be true so that the code knows when to switch between stages
        @param IRQ_src                          
        @return                                 Changes value of button to be true
    '''
    global button
    ##  @brief                                  
    #   @details
    #
    button = True
    
def update_sqw(current_time):
    ''' @brief                                  Generates square wave
        @details
        @param current_time
        @return                                 Current value on square wave
    '''
    return 100*((current_time/1000) % 1 < 0.5)

def update_stw(current_time):
    ''' @brief                                  Generates sawtooth wave
        @details
        @param
        @return                                 Current value on sawtooth wave
    '''
    return 100*((current_time/1000) % 1.0)

def update_sin(current_time):
    ''' @brief                                  Generates sine wave
        @details
        @param
        @return                                 Current value on sine wave
    '''
    return 50*math.sin((current_time/10000) * math.pi*2) + 50
    
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)


if __name__ == '__main__':
    print('Welcome to the terminal. To begin, press the blue user button B1 on the Nucleo. Keep pressing to cycle through LED patterns')
    
    ##  @brief
    #   @details
    #
    runs = 0
   
    ##  @brief
    #   @details
    #
    state = 0
    
    ##  @brief
    #   @details
    #
    button = False
    while(True):
        try:
            if(state == 0):
                #Run state zero code
             
                state = 1
                pass
            elif(state == 1):
                #Run State 1 code
                
                if button == True:
                    state = 2
                    button = False
                    
                    ##  @brief
                    #   @details
                    #
                    startTime = utime.ticks_ms()
                    print('Square wave pattern selected')
                pass
            elif(state == 2):
                #Run State 2 code
                
                ##  @brief
                #   @details
                #
                stopTime = utime.ticks_ms()
                ##  @brief
                #   @details
                #
                duration = utime.ticks_diff(stopTime, startTime)
                #print(update_sqw(duration))
                t2ch1.pulse_width_percent(update_sqw(duration))
                if button == True:
                    state = 3
                    button = False
                    startTime = utime.ticks_ms()
                    print('Sine wave pattern selected')
                pass
            elif(state == 3):
                #Run State 3 code
                
                stopTime = utime.ticks_ms()
                duration = utime.ticks_diff(stopTime, startTime)
                #print(update_sin(duration))
                t2ch1.pulse_width_percent(update_sin(duration))
                if button == True:
                    state = 4
                    button = False
                    startTime = utime.ticks_ms()
                    print('Sawtooth pattern selected')
                pass
            elif(state == 4):
                #Run State 4 code
                
                stopTime = utime.ticks_ms()
                duration = utime.ticks_diff(stopTime, startTime)
                #print(update_stw(duration))
                t2ch1.pulse_width_percent(update_stw(duration))
                if button == True:
                    state = 2
                    button = False
                    startTime = utime.ticks_ms()
                pass
            runs += 1
            time.sleep(0)
        except KeyboardInterrupt:
            break #Breakout of loop on ctrl+C
    
    print('Program Terminating')