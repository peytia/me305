# -*- coding: utf-8 -*-
''' @file                       task_User.py
    @brief                      A task file for interacting with users
    @details                    This task file interacts with users wanting to interact with the hardware and collect data from the data collection task.
                                Allows for the user to balance the table as well as balance the table and ball system, and calibrate the IMU and touch panel.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 21, 2021
'''
import pyb

class User_task:
    '''                                                 @brief                  Interface with users
                                                        @details                This class is used to interact with users wanting to take information off the 
                                                                                data collection task and interact with the system hardware.
    '''
    def __init__(self, calibStateIMU, calibStateTouch, action_Share, gain_Share, balanceMode_Share):
        '''             @brief                      Constructs a user task object
                        @details                    Upon instantiation, the user task object is created. This task is used to interact with the various other tasks in the system.
                        @param calibStateIMU        Used to check the calibration status of the IMU and enter calibration mode if needed.
                        @param calibStateTouch      Used to check the calibration status of the touch panel and enter calibration mode if needed.
                        @param action_Share         The share used to communicate action requests from the user task.
                        @param gain_Share           The share used to communicate the gain and its changes throughout the tasks.
                        @param balaanceMode_Share   The share used to communicate to the controller task and changes which balancing mode the system is in.
        '''
        ## @brief     Instantiates the calibration of the IMU.
        #  @details   Allows for the task to acquire the calibration state of the IMU, as well allowing for the calibration of the IMU.
        # 
        self.imucal = calibStateIMU
        ## @brief     Instantiates the calibration of the IMU.
        #  @details   Allows for the task to acquire the calibration state of the IMU, as well allowing for the calibration of the IMU.
        # 
        self.touchcal = calibStateTouch
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        ## @brief   Variable for the gain share
        #  @details Initializes the gain share and allows for the share to be used within this task.
        #
        self.newKp = gain_Share
        ## @brief   Variable for the balance mode share
        #  @details Initializes the balance mode share and allows for the share to be used within this task.
        #
        self.balanceMode = balanceMode_Share
        ## @brief   Variable for the USB port
        #  @details Initializes the USB port and allows for the code to transfer information through this port
        #
        self.ser_port = pyb.USB_VCP()
        
        
      
        
        print ('Ball Balancer Interface')
        print ('press c ---- Clear fault') 
        print ('press z ---- Enter Calibration Mode for IMU')
        print ('press t ---- Enter Calibration Mode for Touch Panel')
        print ('press g ---- Enter data collection Mode for 10 seconds')
        print ('press s ---- Cancel data collection ')
        print ('press x ---- Center Platform')
        print ('press f ---- Balance Ball')
        print ('press b ---- Apply a Short Bump in Current to motors to Test Function')
        print ('press d ---- Enter Data Collection Mode for 10 Seconds')
        
    def User_input(self):
        '''          @brief              Designates shares based on user input.
                     @details            After the user has been presented with the prompts, user will input the corresponding character
                                         to the desired action. The action share is then modified to be shared to the appropriate task.                                                                              
        '''
        if self.ser_port.any():
            ## @brief    Collects the user input
            #  @details  Once the user is prompted for an input, this variable is changed based off of the input of the user. The shares are then changed based off of the new value of the variable, and program behaves accordingly.
            #
            self.user_in = self.ser_port.read(1)
            print(self.user_in)
            
            if self.user_in == b'c':                                        
                print('Clearing Faults')
                self.action.write(10)  
                
            elif self.user_in == b'z':                                        
                print('Entering IMU Calibration')
                self.imucal.write(True)
                #self.action.write(20)
                
            elif self.user_in == b't':                                      
                print('Entering Touch Panel Calibration')
                self.action.write(30)
                self.touchcal.write(True)
                
            elif self.user_in == b'g':                                        
                print('Entering Data Collection Mode for 10 Seconds')
                self.action.write(40)
                
            elif self.user_in == b's':                                        
                print('Data Collection Cancelled')
                self.action.write(50)
                
            elif self.user_in == b'x':                                        
                print('Centering Platform')
                self.balanceMode.write(1) 
                self.action.write(60)
                
            elif self.user_in == b'f':                                      
                print('Balancing Ball on Platform')
                self.balanceMode.write(2) 
                self.action.write(70)
                
            elif self.user_in == b'b':                                        
                print('Bumping Motors')
                self.action.write(80)
                
            elif self.user_in == b'd':                                        
                print('Collecting Data')
                self.action.write(90)
                
            
                

  
                
                
            

                
                