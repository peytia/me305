''' @file                       task_DataCollection.py
    @brief                      A task file for collecting data gathered by the touch panel and IMU tasks.
    @details                    This task file interacts with the touch panel and IMU tasks, pulling information such as the position of the ball on the platform as well as
                                the angle position of the platform.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       December 8, 2021
'''

import time

class DataCollection_Task:
    '''                  @brief                  Collects data from the system.
                         @details                This class is used to interact with the IMU and touch panel tasks and extrapolates data generated from those tasks.
                                                                               
    '''
    def __init__(self,action_Share, position_Share, Angle_Share):
        '''             @brief                 Constructs a data collection task object
                        @details               Upon instantiation, the data collection task object is created and used to collect and store data 
                                               when requested by the user.
                        @param action_Share    The share used to communicate action requests to the encoder task
                        @param position_Share  The share used to communicate the position of the ball on the touch panel.
                        @param Angle_Share     The share used to communicate the angles from the IMU to the controller task.  
                        
        '''
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        ## @brief   Variable for the position share
        #  @details Initializes the position share and allows for the share to be used within this task.
        #
        self.position = position_Share
        ## @brief     Instantiates the angle share.
        #  @details   Allows for the task to grab information from the driver, as well as manipulate the state of the IMU such as setting operating mode and initialize it.
        # 
        self.angle = Angle_Share
        ## @brief    Variable for the collection state of the task.
        #  @details  Allows for the task to switch in to a collection state and record all data pulled from other tasks.
        #
        self.collectionState = False
        ## @brief    Variable for the timer used during the collection state.
        #  @details  Sets a time for the collection state, limiting the data that is collected and stored.
        #
        self.collectionTimer = 0
        ## @brief    Variable for the timer flip flop.
        #  @details  allows for more data to be stored.
        #
        self.TimFlipFlop = False
               
    def updateDataCollection(self):
        '''             @brief                 Updates the data collection task object
                        @details               Upon recieving instruction from the user to start collecting data, the task instantiates multiple arrays to collect the data 
                                               from the various tasks in the system.
                       
        '''
        if self.action.read() == 90:
            self.action.write(0)
            self.collectionState = True
            ## @brief    Variable for the timer.
            #  @details  Starts the timer.
            #
            self.collectionTimerStart = time.ticks_ms()
            ## @brief    Variable for the ball position on touch panel in the x direction.
            #  @details  Instantiates an empty array for the position data to be stored.
            #
            self.collectedXData = []
            ## @brief    Variable for the ball position on touch panel in the y direction.
            #  @details  Instantiates an empty array for the position data to be stored.
            #
            self.collectedYData = []
            ## @brief    Variable for the angle of the platform about the x-axis.
            #  @details  Instantiates an empty array for the position data to be stored.
            #  
            self.collectedThetaX = []
            ## @brief    Variable for the angle of the platform about the y-axis.
            #  @details  Instantiates an empty array for the position data to be stored.
            #
            self.collectedThetaY = []
            ## @brief    Variable for the time that has passed.
            #  @details  Instantiates an empty array for the time data to be stored.
            #             
            self.collectedTimeData = []
            self.TimFlipFlop = True
            self.collectionTimer = 0
            
            
        if self.collectionState == True and self.collectionTimer < 5000 and self.TimFlipFlop == True:
            ## @brief    Variable for euler angles.
            #  @details  Reads angle data from the angle share and creates a tuple.
            #
            self.eulerTuple = self.angle.read()
            ## @brief    Variable for the ball position.
            #  @details  Reads position data from the position share and creates a tuple.
            #
            self.positionTuple = self.position.read()
            self.collectionTimer = time.ticks_diff(time.ticks_ms(), self.collectionTimerStart)
            self.collectedXData.append(self.positionTuple[0])
            self.collectedYData.append(self.positionTuple[1])
            self.collectedThetaX.append(self.eulerTuple[1])
            self.collectedThetaY.append(self.eulerTuple[2])
            self.collectedTimeData.append(self.collectionTimer)
            print(self.positionTuple[0],self.positionTuple[1],self.eulerTuple[1],self.eulerTuple[2],self.collectionTimer)
            
            
        if self.collectionState == True and self.collectionTimer >= 5000:
            self.collectionState = False
            print('X Data:')
            print(self.collectedXData)
            print('Y Data:')
            print(self.collectedYData)
            print('Theta Y Data')
            print(self.collectedThetaX)
            print('Theta X Data')
            print(self.collectedThetaY)
            print('Time Data')
            print(self.collectedTimeData)
            
        if self.TimFlipFlop == True:
            self.TimFlipFlop = False
        else:
            self.TimFlipFlop = True
