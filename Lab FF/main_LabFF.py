# -*- coding: utf-8 -*-
''' @file                       main_LabFF.py
    @brief                      A main file for balancing a platform and ball system.
    @details                    
This main file initiates and calls on the tasks for performing their respective
tasks with interacting with users and hardware. When ran, it will go to a standby state
where it displays what a user can type into the PuTTY prompt. This includes interacting with the
IMU as well as the motor. The motor position can be plotted by running the motor and then using
the data collection task which generated the following Plots. The primary purpose of this
Lab was to implament a motor speed controller which could modulate the speed of the motor using the
data obtained by the IMU and touch panel to balance a platform and ball system.

While we were not able to completely get the ball balancing perfectly, we were able to achieve key
milestones to accomplish this final goal of balancing the ball. If we were given extra time outside
of finals week in which this project was completed, I believe Caleb and I could have devoted more time
into finetuning the controller and creating an ideal ball balancing response of the machine.

What we did accomplish can be seen in the embedded video. Here, it can be seen that we developed a 
very responsive table balance command. From this, we generated a plot of table angle vs time using a
a data collection task. This can be seen on the page link below. Using the same data collection task
we generated a plot of the machines respose to balancing a ball; however, it generally falls off 
around 3 seconds. This can be seen in the same place here. @ref plotPage

Before this code was created, Finite State Machines were used to lay out the logic of the system being created.
This can be seen with the following images: @ref diagramPage

Here are a few video links demonstrating the usage of our code.

This first video demonstrates the ball balancing using the onboard IMU to prepare for a ball to be placed onto the plate.

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/kAs6H7aRlpQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly

This Next video demonstrates the balance plate responding to the ball moving correctly, but failing to balance the ball due to 
unrefined control loop constants.

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/Jiv7rhiwEyE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly

This Last video demonstrates the process of recording data while the ball is being balanced and how it returns the aquired and stored data

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/Z9u1lKoXMJk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly                                
                                
@author                     Peyton Archibald
@author                     Caleb Kephart
@date                       October 21, 2021
    
    
\page plotPage Response Plot Page
    Here are our response plots for the term project
    
    The first plot is of the table angle position over time. This response is quite quick and
    refined.
    
    \image html TableBalanceAngleResponse.PNG width=700 "Table Balance Angle Response"
    
    The following two plots are of the x and y position of the ball and the table angle of the
    machine trying to correct the position of the ball.
    
    \image html TableAnglePositionBallBalance.PNG width=700 "Ball Balance Position Response"
    
    \image html BallPositionBallBalance.PNG width=700 "Ball Balance Position Response"

\page diagramPage Task Diagram and Finite State Machine Page
    Here are the Finite State Machine Diagrams of all the Tasks and the Task Diagram outlining the function of their role
    in making the machine operable.
    
    Note: Use clrl+scrollWheel to zoom in if Images are too Blurry
    
    \image html LabFF_TaskDiagram.PNG width=700 "Ball Balance TD" 
    \image html LabFF_FSMUser.PNG width=700 "State Machine used for the User Task"
    \image html LabFF_TaskController.PNG width=700 "State Machine used for the Motor Controller Task"
    \image html LabFF_TaskDataCollection.PNG width=700 "State Machine used for the Motor Task"
    \image html LabFF_TaskIMU.PNG width=700 "State Machine used for the Inertial Measurement Unit Task"
    \image html LabFF_TaskMotor.PNG width=700 "State Machine used for the Motor Task"
    \image html LabFF_TaskTouch.PNG width=700 "State Machine used for the Touch Panel Sensor Task"





'''
import task_IMU
import task_user
import task_motor
import task_TouchPanel
import task_Controller
import time
import shares
import task_DataCollection

## @brief     Instantiates action_Share
#  @details   A share for passing request information from the user task to other tasks.
#
action_Share = shares.Share(0)      #A share for passing request information from the user task to encoder task
## @brief     Instantiates calibStateIMU
#  @details   A share for passing the calibration state of the IMU.
#
calibStateIMU = shares.Share(0)
## @brief     Instantiates calibStateTouch
#  @details   A share for passing the calibration state of the Touch Panel.
#
calibStateTouch = shares.Share(0)
## @brief     Instantiates Angle_Share
#  @details   A share for passing the angles of the platform from the IMU to other tasks.
#
Angle_Share = shares.Share(0)
## @brief     Instantiates position_Share
#  @details   A share for passing the position of an object on the touch panel to other tasks.
#
position_Share = shares.Share(0)
## @brief     Instantiates gain_Share
#  @details   A share for passing the gains required to balance the system to other tasks.
#
gain_Share = shares.Share(0)
## @brief     Instantiates balanceMode_Share
#  @details   A share for passing the current balancing mode of the platform, either balancing the platform itself or the platform with the ball.
#
balanceMode_Share = shares.Share(0)
## @brief    Instantiates the user task
#  @details  Passing calibStateIMU, calibStateTouch, action_Share, gain_Share, and balanceMode_Share into the user task.
#
user = task_user.User_task(calibStateIMU, calibStateTouch, action_Share, gain_Share, balanceMode_Share)            #passing action_share into both tasks
## @brief    Instantiates the motor task
#  @details  Passing action_Share, gain_Share, and balanceMode_Share into the motor task.
#
motor = task_motor.Motor_task(action_Share, gain_Share, balanceMode_Share)
## @brief    Instantiates the user task
#  @details  Passing calibStateIMU, and Angle_Share into the imu task.
#
imu = task_IMU.IMU_Task(calibStateIMU, Angle_Share)
## @brief    Instantiates the user task
#  @details  Passing calibStateTouch, position_Share, and action_Share into the touch panel task.
#
touch = task_TouchPanel.Touch_Task(calibStateTouch, position_Share, action_Share)
## @brief    Instantiates the controller task
#  @details  Passing calibStateIMU, calibStateTouch, Angle_Share, gain_Share, balanceMode_Share, and position_Share into the controller task.
#
control = task_Controller.Controller_Task(calibStateIMU, calibStateTouch, Angle_Share, gain_Share, balanceMode_Share, position_Share)
## @brief    Instantiates the data collection task
#  @details  Passing action_Share, position_Share, and Angle_Share into the data task.
#
dataCollection = task_DataCollection.DataCollection_Task(action_Share, position_Share, Angle_Share)

if __name__ == '__main__':    
    while True:         #main while loop
        ## @brief Starts the timer for the loop.
        #
        startTime = time.ticks_us()
        user.User_input()
        touch.updateTouch()
        imu.updateIMU()
        control.updateGain()
        motor.motor_update()
        dataCollection.updateDataCollection()
        ## @brief regulates time of tasks to 20Hz
        #
        stopTime = time.ticks_us()
        time.sleep_us(20000-time.ticks_diff(stopTime,startTime)) #used for regulating time of tasks to 20Hz