# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 13:38:08 2021

@author: melab15
"""

# -*- coding: utf-8 -*-
''' @file BNO055.py
'''
import pyb
import struct
import time
class BNO055:
    ''' @brief      A driver class for the IMU onboard.
        @details    Allows for data to be collected from the IMU.                 
    '''
    
    def __init__ (self, I2C_Channel):
        ''' @brief      Initializes and returns a BNO055 object.
        '''
        ## @brief  Instantiates the I2C.
        #
        self.i2c = pyb.I2C(I2C_Channel, pyb.I2C.MASTER)
    def oper_mode (self, operation_mode, I2C_Address):
        ''' @brief      Initializes and returns a operating mode object.
        '''
        self.i2c.mem_write(operation_mode, I2C_Address, 0x3D)
        
        
        
        pass
    def calibStatus (self):
        ''' @brief      Determines the calibration status of the IMU.
        '''
        ## @brief bytes for the calibration process.
        #
        cal_bytes =  self.i2c.mem_read(1, 0x28, 0x35)

        ## @brief byte array for the calibration status.
        #
        cal_status = ( cal_bytes[0] & 0b11,
                       (cal_bytes[0] & 0b11 << 2) >> 2,
                       (cal_bytes[0] & 0b11 << 4) >> 4,
                       (cal_bytes[0] & 0b11 << 6) >> 6)

        print('\n')

        return cal_status

    def calibConstant (self):
        ''' @brief      obtains the calibration constants.
        '''
        ## @brief  byte array for calibration readings.
        #
        self.bufcal = bytearray(22)
        return self.i2c.mem_read(self.bufcal, 0x28, 0x55)
#    def calibWrite (self, cal_con):
#        cal_bytes = bytearray(cal_con)
    
    def EulerAngle (self):
        ''' @brief      Initializes euler angle readings.
            @return     Euler Angles
        '''
        ## @brief  byte array for euler angle readings.
        #
        self.buffer_euler = bytearray(6)
        ## @brief hex values for the euler angles.
        #
        self.eulerAng_hexval = self.i2c.mem_read(self.buffer_euler, 0x28, 0x1A)
        ## @breif unscaled eauler angles
        #
        self.eulerAng_unscaled = struct.unpack('<hhh', self.eulerAng_hexval)
        ## @brief tuple of the eauler angles recieved from the IMU.
        #
        self.eulerAng = tuple(eul_int/16 for eul_int in self.eulerAng_unscaled)
    
    

        return self.eulerAng
    
    def Acceleration (self):
        ''' @brief      Initializes accelerometer.
            @return     accelerometer
        '''
        ## @brief  byte array for accelerometer readings.
        #
        self.buffer_accel = bytearray(6)
        ## @brief hex values for the euler angles.
        #
        self.acc_hexval = self.i2c.mem_read(self.buffer_accel, 0x28, 0x08)
        ## @breif unscaled eauler angles
        #
        self.acc_unscaled = struct.unpack('<hhh', self.acc_hexval)
        ## @brief tuple of the eauler angles recieved from the IMU.
        #
        self.acc = tuple(acc_int/16 for acc_int in self.acc_unscaled)
        
        return self.acc


    
if __name__ == '__main__':
    import pyb
    import struct
    import time

    
    
    Accell = BNO055(1)
    Accell.oper_mode(12,0x28)
    state = 0
    while True:
        
        if state == 0: 
            Calibration_Status = Accell.calibStatus()
            print(Calibration_Status)
            if Calibration_Status == (3,3,3,3):
                print('Fully Calibrated')
                state = 1
        if state == 1:
            eulerData = Accell.EulerAngle()
            print(eulerData)
        time.sleep_ms(500)
        