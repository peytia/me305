# -*- coding: utf-8 -*-
''' @file                       task_Controller.py
    @brief                      A task file for putting the system into a closed loop.
    @details                    This task file interacts with the information gathered from the touch panel and IMU tasks to determine the gains that are 
                                required to maintain the balance of the system. The task then shares those gains with the motor task to allow for the motors 
                                to stabalize the system.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       Dec 2, 2021
'''

import pyb
import time


class Controller_Task:
    ''' @brief      A controller class to implement a closed loop on the system.
      
    '''
    
    def __init__(self,calibStateIMU, calibStateTouch, Angle_Share, gain_Share, balanceMode_Share, position_Share):
        ''' @brief                      Constructs a controller task object
            @details                    Utilizes data gained from other tasks to determine proper gains that are implemented to the motor task to balance the system.
            @param calibStateIMU        Used to check the calibration status of the IMU and enter calibration mode if needed.
            @param calibStateTouch      Used to check the calibration status of the touch panel and enter calibration mode if needed.
            @param Angle_Share          The share used to communicate the angles from the IMU to the controller task.  
            @param gain_Share           The share used to communicate the gain and its changes throughout the tasks.
            @param balaanceMode_Share   The share used to communicate to the controller task and changes which balancing mode the system is in.
            @param position_Share       The share used to communicate the position of the ball on the touch panel.          
        '''
       
        ## @brief   Variable for the gain share
        #  @details Initializes the gain share and allows for the share to be used within this task.
        #
        self.gain = gain_Share
        ## @brief     Instantiates the calibration of the IMU.
        #  @details   Allows for the task to acquire the calibration state of the IMU, as well allowing for the calibration of the IMU.
        # 
        self.touchcal = calibStateTouch
        ## @brief     Instantiates the calibration of the IMU.
        #  @details   Allows for the task to acquire the calibration state of the IMU, as well allowing for the calibration of the IMU.
        # 
        self.imucal = calibStateIMU
        ## @brief   Variable for the balance mode share
        #  @details Initializes the balance mode share and allows for the share to be used within this task.
        #
        self.balanceMode = balanceMode_Share
        ## @brief     Instantiates the angle share.
        #  @details   Allows for the task to grab information from the driver, as well as manipulate the state of the IMU such as setting operating mode and initialize it.
        # 
        self.angleShare = Angle_Share
        ## @brief   Variable for the position share
        #  @details Initializes the position share and allows for the share to be used within this task.
        #
        self.position = position_Share
        ## @brief   Last point in time recorded.
        #
        self.timeLast = time.ticks_us()
        ## @brief   Variable for the angle of the platform about the y-axis.
        #  @details Records the last angle of the platform about the y-axis.
        #
        self.thetaYLast = 0
        ## @brief   Variable for the angle of the platform about the x-axis.
        #  @details Records the last angle of the platform about the x-axis.
        #
        self.thetaXLast = 0
        ## @brief   Variable for the touch pad position on the x-axis.
        #  @details Records the last touch pad position on the x-axis.
        #
        self.Xlast = 0
        ## @brief   Variable for the touch pad position on the y-axis.
        #  @details Records the last touch pad position on the y-axis.
        #
        self.Ylast = 0
        
    def updateGain (self):
        '''             @brief                 Updates the gain task object
                        @details               Upon recieving data from the IMU and touch screen tasks, the controller task is updated and provides new gaisn that
                                               will be sent to the motors to balance the system.                       
        '''
        if self.balanceMode.read() > 0:
            
            #Getting DeltaT for velocity Calcs
            
            ## @brief   Variable records current time.
            #             
            self.timeNow = time.ticks_us()
            ## @brief   Variable records difference in time between current and last recorded instance of time.
            #
            self.deltaT = time.ticks_diff(self.timeNow,self.timeLast)/1000000
            self.timeLast = self.timeNow
            
            #Pulling Angle Position Data
            
            ## @brief Tuple of the angles from the IMU
            #
            self.eulerTuple = self.angleShare.read()
            ## @brief Stores the value for the angle about the y-axis.
            #
            self.thetaY = self.eulerTuple[2]
            ## @brief Stores the value for the angle about the x-axis.
            #
            self.thetaX = self.eulerTuple[1]
            
            #Calculating Angular Velocity/Theta Dot
            ## @brief Records the angular velocity of the platform about the y-axis.
            #
            self.thetaY_dot = (self.thetaY-self.thetaYLast)/self.deltaT
            self.thetaYLast = self.thetaY
            ## @brief Records the angular velocity of the platform about the x-axis.
            #
            self.thetaX_dot = (self.thetaX-self.thetaXLast)/self.deltaT
            self.thetaXLast = self.thetaX
            
            #Getting Ball Position
            
            ## @brief Tuple of the ball position from the touch screen.
            #
            self.positionTuple = self.position.read()
            ## @brief Stores the value for the position on x-axis.
            #
            self.X = self.positionTuple[0]
            ## @brief Stores the value for the position on y-axis.
            #
            self.Y = self.positionTuple[1]
            
            #Calculating Ball Velocity/XDot
            
            ## @brief Records the velocity of the ball along the x-axis.
            #        
            self.X_dot = (self.X - self.Xlast)/self.deltaT
            self.Xlast = self.X
            ## @brief Records the velocity of the ball along the y-axis.
            #    
            self.Y_dot = (self.Y - self.Ylast)/self.deltaT
            self.Ylast = self.Y

            #print(self.thetaY,self.thetaX)
        if self.balanceMode.read() == 1:
            if self.thetaY > 5:
                self.thetaY = 5
            if self.thetaY < -5:
                self.thetaY = -5
            ## @brief     Gain for motor to move x-axis. 
            #  @details   Gain that has been determined from the data and will be used to set the motor PWM to level the platform on the x-axis.
            #
            self.gainX = self.thetaY*14 + 1/5*self.thetaY_dot
            
            if self.thetaX > 5:
                self.thetaX = 5
            if self.thetaX < -5:
                self.thetaX = -5
            ## @brief     Gain for motor to move y-axis. 
            #  @details   Gain that has been determined from the data and will be used to set the motor PWM to level the platform on the y-axis.
            #    
            self.gainY = -(self.thetaX*14 + 1/5*self.thetaY_dot)
            ## @brief    Tuple of the x and y gains used to level the platform.
            #
            self.gainTuple = tuple((self.gainX,self.gainY))
            self.gain.write(self.gainTuple)
            
        elif self.balanceMode.read() == 2 and self.positionTuple[2] == 1:
            self.gainX = ( 1/20)*self.X + ( 8)*self.thetaY + ( 1/3000)*self.X_dot + ( 1/2)*self.thetaY_dot
            self.gainY = (-1/20)*self.Y + (-8)*self.thetaX + (-1/3000)*self.Y_dot + (-1/2)*self.thetaX_dot
            print(( 1/20)*self.X, ( 6)*self.thetaY, ( 1/1500)*self.X_dot, ( 1/6)*self.thetaY_dot)
            self.gainTuple = tuple((self.gainX,self.gainY))
            self.gain.write(self.gainTuple)
        else:
            self.gainTuple = tuple((0,0))
            self.gain.write(self.gainTuple)
            