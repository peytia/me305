''' @file TouchPanel.py
    @brief                      A driver for the touch panel
    @details                    This is a driver for interfacing with the touch panel. Takes in inputs to correctly configure the pins and channels.
                                as well as output the position information of the touch panel. Able to determine if there is an object in contact with the touch panel
                                and determine its position relative to its axis.
                                 
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       November 18, 2021
'''



import pyb
import time

class TouchPanel:
    ''' @brief      A driver class for the Touch panel.
        @details    Sets pins to allow for interfacing with the touch panel task as well as sharing data with the task.
    '''

    def __init__ (self):
        ''' @brief      Initializes a Touch Panel object.
            @details    Sets pins for x y and z scans.
        ''' 
        
        ## @brief Instantiates the ym pin for the touch panel
        #
        self.pin_ym = pyb.Pin.cpu.A0
        ## @brief Instantiates the xm pin for the touch panel
        #
        self.pin_xm = pyb.Pin.cpu.A1
        ## @brief Instantiates the yp pin for the touch panel
        #
        self.pin_yp = pyb.Pin.cpu.A6
        ## @brief Instantiates the xp pin for the touch panel
        #
        self.pin_xp = pyb.Pin.cpu.A7
        
    
    def xscan (self):
        ''' @brief      Gathers the x position of object on the touch panel.
            @return     x position.
        ''' 
        ## @brief Variable for the xm readings. Can be set to high or low depending on what coordinate needs to be read. 
        #
        self.xm = pyb.Pin(self.pin_xm, mode=pyb.Pin.OUT_PP)
        ## @brief Variable for the xp readings. Can be set to high or low depending on what coordinate needs to be read. 
        #
        self.xp = pyb.Pin(self.pin_xp, mode=pyb.Pin.OUT_PP)
        ## @brief Variable for the yp readings. Can be set to high or low depending on what coordinate needs to be read. 
        #
        self.yp = pyb.Pin(self.pin_yp, mode=pyb.Pin.IN)
        ## @brief Variable for the ym readings. Can be set to high or low depending on what coordinate needs to be read. 
        #
        self.ym = pyb.ADC(self.pin_ym)
        
        
        self.xp.high()
        self.xm.low()
        
        
        
        return self.ym.read()
    
    
    def yscan (self):
        ''' @brief      Gathers the y position of object on the touch panel.
            @return     y position.
        ''' 
        self.yp = pyb.Pin(self.pin_yp, mode=pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(self.pin_ym, mode=pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.pin_xp, mode=pyb.Pin.IN)
        self.xm = pyb.ADC(self.pin_xm)
        
        
        self.yp.high()
        self.ym.low()
        
        
        
        return self.xm.read()
    
    def zscan (self):
        ''' @brief      Gathers the z position of object on the touch panel.
            @return     z position.
        ''' 
        self.yp = pyb.Pin(self.pin_yp, mode=pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(self.pin_xm, mode=pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.pin_xp, mode=pyb.Pin.IN)
        self.ym = pyb.ADC(self.pin_ym)
        
        
        self.yp.high()
        self.xm.low()
        
        
        
        return self.ym.read()
    
