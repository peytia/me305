''' @file                       task_TouchPanel.py
    @brief                      A task file for extracting data from the touch panel.
    @details                    This task file interacts with the data recieved from the touch panel driver and manipulates it so that the 
                                controller task can make proper adjustments to the motor gains and balance the system.
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       Dec 2, 2021
'''


import TouchPanel
import time


class Touch_Task:
    '''                  @brief                  Collects data from the touch screen driver.
                         @details                This class is used to interact with the touch panel tasks and sends that data to the controller task.
                                                                               
    '''
    
    def __init__(self,calibStateTouch, position_Share, action_Share):
        '''             @brief                 Constructs a touch panel task object
                        @details               Upon instantiation, the touch panel task object is created and used to collect data from the driver.
                        @param calibStateTouch      Used to check the calibration status of the touch panel and enter calibration mode if needed.
                        @param position_Share  The share used to communicate the position of the ball on the touch panel.
                        @param action_Share    The share used to communicate action requests from the user task.
                    
        '''
        ## @brief     Instantiates the touch panel driver.
        #  @details   Allows for the task to grab information from the driver.
        #  
        self.touchPanel = TouchPanel.TouchPanel()
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.touchcal = calibStateTouch
        ## @brief   Variable for the position share
        #  @details Initializes the position share and allows for the share to be used within this task.
        #
        self.position = position_Share
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        ## @brief   Flag that 'freezes' the calibration process.
        #  @details Prevents from multiple entries being added rapidly to the calibration arrays. Acts almost like a filter.
        #
        self.touchCalFreeze = False
        ## @brief Timer for the 'freezing' process.
        #
        self.FreezeTimerStart = 0
        ## @brief  Determines the number of entries for the calibration arrays.
        #
        self.n = 0
        ## @brief   Array for the x positions.
        #  @details Array for x positions neededfor the calibration process.
        #
        self.x = []
        ## @brief   Array for the y positions.
        #  @details Array for y positions neededfor the calibration process.
        #
        self.y = []
    
        
    def updateTouch (self):
        '''             @brief                 Updates the touch panel task object
                        @details               Upon recieving instruction from the user to start calibrating the touch panel, the task collects the position data
                                               recieved from the panel driver and updates other tasks.
                       
        '''
        ## @brief Readings of the x-position.
        #
        xpos = self.touchPanel.xscan()-2000
        ## @brief Readings of the y-position.
        #
        ypos = self.touchPanel.yscan()-1990
        ## @brief Readings of the z-position.
        #
        zread = self.touchPanel.zscan()
        if zread >= 4000:
            zpos = 0
        else:
            zpos = 1
        ## @brief tuple of the position data from the touch panel.
        #
        self.positionTuple = tuple((xpos, ypos, zpos))
        
        self.position.write(self.positionTuple)

        if self.touchcal.read() == True:        #print(xpos, ypos, zpos)

            ## @brief  Variable for the z position. Used to determine if ball is on the platform.
            #
            z = zpos
            if z == 1 and self.touchCalFreeze == False:
                self.x.append(xpos)
                self.y.append(ypos)
                
                self.n += 1
                print(self.x,self.y)
                self.touchCalFreeze = True
                self.FreezeTimerStart = time.ticks_ms()

                
            if self.n == 4:
                self.n = 0
                print('4 coordinates selected. Calibration complete')
                self.x = []
                self.y = []
                
                self.touchcal.write(False)
            
            if time.ticks_diff(time.ticks_ms(),self.FreezeTimerStart) > 1000:
                self.touchCalFreeze = False
                
            