# -*- coding: utf-8 -*-
''' @file                       task_Motor.py
    @brief                      A task file for interacting with motors
    @details                    This task file interacts with users wanting to take information off of an
                                encoder. This gives instruction to the encoder task which then interacts with
                                the encoder hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       October 28, 2021
'''
import pyb
import DRV8847
import time

class Motor_task:
    ''' @brief                  Interface with motors
        @details                This class is used to perform the actions requested by the user task to
                                interact with motor hardware
    '''
    def __init__(self, action_Share, gain_Share, balanceMode_Share):
        ''' @brief                      Constructs a motor task object
            @details                    Determines the PWM duty cycles that are sent to the motors.
            @param action_Share         The share used to communicate action requests from the user task 
            @param gain_Share           The share used to communicate the gain and its changes throughout the tasks.
            @param balaanceMode_Share   The share used to communicate to the controller task and changes which balancing mode the system is in.
        '''
       
        #share initialization
        ## @brief   Variable for the action share
        #  @details Initializes the action share and allows for the share to be used within this task.
        #
        self.action = action_Share
        
        self.gain = gain_Share
        
        self.balanceMode = balanceMode_Share
        
        #motor driver and motor instantiation
        ## @brief   Motor Driver
        #  @details Instantiates the motor driver, and allows for it to be manipulated through this task.
        #
        self.motor_drv     = DRV8847.DRV8847(pyb.Pin.cpu.A15)                   #first DRV is file name, second DRV is class instantiation
        ## @brief   Motor x
        #  @details Instantiates the 1st motor allows for the duty cycle for the motor to be set.
        #
        self.motor_x       = self.motor_drv.motor(pyb.Pin.cpu.B4,pyb.Pin.cpu.B5, 1, 2)
        ## @brief   Motor y
        #  @details Instantiates the 1st motor allows for the duty cycle for the motor to be set.
        #
        self.motor_y       = self.motor_drv.motor(pyb.Pin.cpu.B0,pyb.Pin.cpu.B1, 3, 4)
        
        #bump check instantiation
        ## @brief   Applies small duty to motors to verify direction.
        #
        self.motorBumpState = False

        
    def motor_update(self):
        ''' @brief              Motor update 
            @details            Updates the duty cycles of the motors based off of the gains recieved from the controller task.
        '''
        if self.action.read() == 80:             #the bumping motors
            self.action.write(0)                #clear the share if flag is seen
            self.motorBumpState = True
            ##  @brief start timer for the bump process.
            #
            self.bumpStartTime = time.ticks_ms()
        
        if self.action.read() == 62:            #the action for Zeroing encoder 2, checking the share value
            self.action.write(0)                #clear the share if flag is seen
            self.motor_2.set_duty(self.duty.read())    #action
            
        if self.action.read() == 7:             #Clear Fault in Motor
            self.motor_drv.fault_cb(0)
            
        if self.motorBumpState == True:
            self.bumpCurrentTime = time.ticks_diff(time.ticks_ms(),self.bumpStartTime)
            if self.bumpCurrentTime > 1000 and self.bumpCurrentTime < 1200:
                self.motor_x.set_duty(80)
                self.motor_y.set_duty(80)
            elif self.bumpCurrentTime > 1200 and self.bumpCurrentTime < 2000:
                self.motor_x.set_duty(0)
                self.motor_y.set_duty(0)
            elif self.bumpCurrentTime > 2000 and self.bumpCurrentTime < 2200:
                self.motor_x.set_duty(-80)
                self.motor_y.set_duty(-80)
            elif self.bumpCurrentTime > 2200:
                self.motor_x.set_duty(0)
                self.motor_y.set_duty(0)
                self.motorBumpState = False
                
        if self.balanceMode.read() > 0:
            ## @brief  Tuple of the gains from the controller task.
            #
            self.gainTuple = self.gain.read()
            ## @brief Gain for the x-direction.
            #
            self.gainX = self.gainTuple[0]
            if self.gainX > 100:
                self.gainX = 100
            if self.gainX < -100:
                self.gainX = -100
            self.motor_x.set_duty(self.gainX)
            ## @brief Gain for the y-direction.
            #
            self.gainY = self.gainTuple[1]
            if self.gainY > 100:
                self.gainY = 100
            if self.gainY < -100:
                self.gainY = -100
            self.motor_y.set_duty(self.gainY)
    
                
            
   