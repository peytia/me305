
''' @file                       task_IMU.py
    @brief                      A task file for interacting with the user interface and controller utilizing the IMU.
    @details                    This task file interacts with users wanting to balance the platform and obtain data such as changes in the angle of the platform. 
                                the IMU hardware
                                
    @author                     Peyton Archibald
    @author                     Caleb Kephart
    @date                       December 2, 2021
'''

import BNO055
import pyb
import time

class IMU_Task:
    '''                  @brief                  Interface with IMU driver
                         @details                This class is used to interact with the driver and collects data used to balance the system.
                                                                               
    '''
    def __init__(self, calibStateIMU, Angle_Share):
        '''             @brief                Constructs an IMU task object.
                        @details              Upon instantiation, the IMU task object is created.
                        @param calibStateIMU  Used to check the calibration status of the IMU and enter calibration mode if needed.
                        @param Angle_Share    The share used to communicate the angles from the IMU to the controller task.                        
        '''
        ## @brief     Instantiates the IMU driver.
        #  @details   Allows for the task to grab information from the driver, as well as manipulate the state of the IMU such as setting operating mode and initialize it.
        #  
        self.Accell = BNO055.BNO055(1)
        self.Accell.oper_mode(12,0x28)
        ## @brief     Instantiates the calibration of the IMU.
        #  @details   Allows for the task to acquire the calibration state of the IMU, as well allowing for the calibration of the IMU.
        # 
        self.calibStateIMU = calibStateIMU
        ## @brief     Instantiates the angle share.
        #  @details   Allows for the task to grab information from the driver, as well as manipulate the state of the IMU such as setting operating mode and initialize it.
        # 
        self.angleShare = Angle_Share
    
    def updateIMU (self):
        '''             @brief                Updates the IMU task object.
                        @details              Updates the calibration status of the IMU as well as the euler angles recieved from the IMU driver.          
        '''
        if self.calibStateIMU.read() == True:
            ## @brief     Checks IMU calibration status.
            #  @details  Checks the IMU calibration status. Calibrates the IMU if needed.
            # 
            self.Calibration_Status = self.Accell.calibStatus()
            print(self.Calibration_Status)
            if self.Calibration_Status == (3,3,3,3):
                print('Fully Calibrated')
                self.calibStateIMU.write(False)
        if self.calibStateIMU.read() == False:
            self.angleShare.write(self.Accell.EulerAngle())

            